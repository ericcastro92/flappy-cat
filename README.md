# Flappy Cat
Flappy Bird clone for Sketch 2.
## Instructions
Press the green flag to start the game. Avoid the green pipes by pressing the space bar to "fly". You get a point for each pipe you successfully move through. The game is over when you hit a pipe.
